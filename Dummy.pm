package Business::OnlinePayment::Dummy;

use strict;
use Carp;
use Business::OnlinePayment;
use vars qw($VERSION @ISA $me);

@ISA = qw(Business::OnlinePayment);
$VERSION = '0.01';
$me = 'Business::OnlinePayment::Dummy';

my %failure_status = (
  1 => 'expired',
  2 => 'nsf',
  3 => 'stolen',
  4 => 'pickup',
  5 => 'blacklisted',
  6 => 'declined',
);


sub submit {
    my($self) = @_;
    $self->order_number(time . '-' . int(rand(10000)));
    my %c = $self->content;
    if ( $c{'amount'} > 1.00 ) {
      $self->is_success(1);
      $self->authorization(int(rand(1000000)));
    } elsif ( $c{'amount'} > 0.00 ) {
      $self->is_success(0);
      my $status = sprintf('%.0f', $c{'amount'} * 100);
      if ($failure_status{$status}) {
        $self->failure_status( $failure_status{$status} );
        $self->error_message( uc($failure_status{$status}) );
      }
    } else {
      die "Processor error";
    }
    return;
}

1;

__END__

=head1 NAME

Business::OnlinePayment::Dummy - Test backend for Business::OnlinePayment

=head1 SYNOPSIS

Submit returns success if the amount field is at least 1.00. If it's not,
instead returns the following status based on the cents part:

.01: expired
.02: nsf (non-sufficient funds)
.03: stolen
.04: pickup
.05: blacklisted
.06: declined

.00 throws an exception, to simulate a communication failure.

=head1 COPYRIGHT & LICENSE

Copyright 2016 Freeside Internet Services, Inc.

This program is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

=head1 SEE ALSO

perl(1). L<Business::OnlinePayment>.

=cut

